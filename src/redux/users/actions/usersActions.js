import {ActionTypes} from "../contants/action-types";

export const setUser =  (products) => {
    return {
        type: ActionTypes.SET_USER,
        payload: products
    }
}
export const setToken =  (products) => {
    return {
        type: ActionTypes.SET_TOKEN,
        payload: products
    }
}
export const setIsLogged =  (products) => {
    return {
        type: ActionTypes.SET_IS_LOGGED,
        payload: products
    }
}
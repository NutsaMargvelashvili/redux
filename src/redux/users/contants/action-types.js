export const ActionTypes = {
    SET_USER: "SET_USER",
    SET_TOKEN: "SET_TOKEN",
    SET_IS_LOGGED: "SET_IS_LOGGED",
}
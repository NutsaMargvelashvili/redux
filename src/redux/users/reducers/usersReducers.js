import {ActionTypes} from "../contants/action-types";

const initialState = {
    users: {
        user: [],
        token: ''
    },
    isLogged: false,
}
export const usersReducer = (state = initialState, {type, payload}) => {
    switch (type){
        case ActionTypes.SET_USER:
            return {
                ...state,
                users:{
                    ...state.users,
                    user: payload
                }
            };
        case ActionTypes.SET_TOKEN:
            return {
                ...state,
                users:{
                    ...state.users,
                    token: payload
                }
            };
        case ActionTypes.SET_IS_LOGGED:
            return {
                ...state,
                isLogged: payload
            };
        default:
            return state;
    }
}

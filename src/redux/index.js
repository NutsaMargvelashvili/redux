import {combineReducers} from "redux";
import {productReducer} from "./products/reducers/productReducer";
import {countriesReducer} from "./countries/reducers/countriesReducer";
import {usersReducer} from "./users/reducers/usersReducers";

const reducers = combineReducers({
    products: productReducer,
    countries: countriesReducer,
    users: usersReducer
})

export default reducers;
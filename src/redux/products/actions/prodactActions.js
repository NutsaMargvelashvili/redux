import {ActionTypes} from "../contants/action-types";

export const setProducts =  (products) => {
    return {
        type: ActionTypes.SET_PRODUCTS,
        payload: products
    }
}

export const selectedProducts =  (product) => {
    return {
        type: ActionTypes.SELECTED_PRODUCT,
        payload: product
    }
}
export const checkedProducts =  (product) => {
    return {
        type: ActionTypes.CHECKED_PRODUCTS,
        payload: product
    }
}
export const selectOrDeselect =  (products) => {
    return {
        type: ActionTypes.SELECT_OR_DESELECT,
        payload: products
    }
}
export const filterProducts =  (products) => {
    return {
        type: ActionTypes.FILTER_PRODUCTS,
        payload: products
    }
}

export const searchProducts =  (products) => {
    return {
        type: ActionTypes.SEARCH_PRODUCTS,
        payload: products
    }
}
export const sortProducts =  (products) => {
    return {
        type: ActionTypes.SORT_PRODUCTS,
        payload: products
    }
}
export const minPrice =  (products) => {
    return {
        type: ActionTypes.SET_MIN_PRICE,
        payload: products
    }
}
export const maxPrice =  (products) => {
    return {
        type: ActionTypes.SET_MAX_PRICE,
        payload: products
    }
}
export const resetFilter =  (products) => {
    return {
        type: ActionTypes.RESET_FILTER,
        payload: products
    }
}
export const selectCategory =  (products) => {
    return {
        type: ActionTypes.SELECTED_CATEGORY,
        payload: products
    }
}
export const addToInventory =  (products) => {
    return {
        type: ActionTypes.INVENTORY,
        payload: products
    }
}
export const addCheckedToInventory =  (products) => {
    return {
        type: ActionTypes.CHECKED_INVENTORY,
        payload: products
    }
}
export const checkedInventoryProducts =  (product) => {
    return {
        type: ActionTypes.CHECKED_INVENTORY_PRODUCTS,
        payload: product
    }
}
export const selectOrDeselectInventory =  (products) => {
    return {
        type: ActionTypes.SELECT_OR_DESELECT_INVENTORY,
        payload: products
    }
}
export const removeFromInventory =  (products) => {
    return {
        type: ActionTypes.REMOVE_FROM_INVENTORY,
        payload: products
    }
}
export const filterInventory =  (products) => {
    return {
        type: ActionTypes.FILTER_INVENTORY,
        payload: products
    }
}
export const searchInventory =  (products) => {
    return {
        type: ActionTypes.SEARCH_INVENTORY,
        payload: products
    }
}
export const sortInventory =  (products) => {
    return {
        type: ActionTypes.SORT_INVENTORY,
        payload: products
    }
}
export const minPriceInventory =  (products) => {
    return {
        type: ActionTypes.SET_MIN_PRICE_INVENTORY,
        payload: products
    }
}
export const maxPriceInventory =  (products) => {
    return {
        type: ActionTypes.SET_MAX_PRICE_INVENTORY,
        payload: products
    }
}
export const setAlert =  (products) => {
    return {
        type: ActionTypes.SET_ALERT,
        payload: products
    }
}
export const setAlertType =  (products) => {
    return {
        type: ActionTypes.SET_ALERT_TYPE,
        payload: products
    }
}

import {ActionTypes} from "../contants/action-types";
import {filter} from "../../../filter";
import {priceRange} from "../../../components/priceRange";

const initialState = {
    products: [],
    filteredProducts: [],
    singleProduct : null,
    checkedProductsList: [],
    isLoading: true,
    sliderRange: {
        min: 0,
        max: 0
    },
    filterProductList:{
        sort: 'New',
        search: '',
        slideMin: 0,
        slideMax: 0,
        category: ''
    },
    inventory: [],
    filteredInventory: [],
    checkedInventoryList: [],
    inventorySliderRange: {
        min: 0,
        max: 0
    },
    filterInventoryList:{
        sort: 'New',
        search: '',
        slideMin: 0,
        slideMax: 0,
        category: ''
    },
    alert: "",
    alertType: ""
}

export const productReducer = (state = initialState, {type, payload}) => {

    switch (type){
        case ActionTypes.SET_PRODUCTS:
             let minPrice = priceRange(payload).min
             let maxPrice = priceRange(payload).max
            return {...state,
                products: payload,
                filteredProducts: payload,
                filterProductList:{
                    ...state.filterProductList,
                    slideMin: minPrice,
                    slideMax: maxPrice
                },
                sliderRange: {
                    min: minPrice,
                    max: maxPrice
                },
                isLoading: false
            };
        case ActionTypes.SELECTED_PRODUCT:
            return {
                ...state,
                singleProduct: payload
            };
        case ActionTypes.CHECKED_PRODUCTS:

            const prods = state.checkedProductsList.includes(payload) ?
                state.checkedProductsList.filter(p => p.id !== payload.id) :
                [...state.checkedProductsList , payload]
            return {
                ...state,
                singleProduct : null,
                checkedProductsList: prods
            };
        case ActionTypes.SELECT_OR_DESELECT:
            return {
                ...state,
                checkedProductsList: payload
            };
        case ActionTypes.SEARCH_PRODUCTS:

            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    search: payload
                }
            };
        case ActionTypes.SORT_PRODUCTS:
            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    sort: payload
                }
            };
        case ActionTypes.SET_MIN_PRICE:
            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    slideMin: payload
                }
            };
        case ActionTypes.SET_MAX_PRICE:
            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    slideMax: payload
                }
            };
        case ActionTypes.FILTER_PRODUCTS:
            return {
                ...state,
                filteredProducts:  filter(state.products, state.filterProductList),
                checkedProductsList: []
            };
        case ActionTypes.RESET_FILTER:
            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    slideMax: state.sliderRange.max,
                    slideMin: state.sliderRange.min,
                    category: ''
                }
            };
        case ActionTypes.SELECTED_CATEGORY:

            return {
                ...state,
                filterProductList:{
                    ...state.filterProductList,
                    category: payload
                }
            };
        case ActionTypes.INVENTORY:
            let minInventoryPrice = priceRange(payload).min
            let maxInventoryPrice = priceRange(payload).max
            return {
                ...state,
                singleProduct : null,
                inventory: payload,
                filteredInventory: payload,
                filterInventoryList:{
                    ...state.filterInventoryList,
                    slideMin: minInventoryPrice,
                    slideMax: maxInventoryPrice
                },
                inventorySliderRange: {
                    min: minInventoryPrice,
                    max: maxInventoryPrice
                },
            };
        case ActionTypes.CHECKED_INVENTORY:
            const checkedInventory = state.inventory.includes(state.checkedProductsList) ?
                state.inventory.filter(p => p.id !== state.checkedProductsList.id) :
                [...state.inventory , ...state.checkedProductsList]
            return {
                ...state,
                inventory: checkedInventory
            };
        case ActionTypes.CHECKED_INVENTORY_PRODUCTS:

            const inventoryProducts = state.checkedInventoryList.includes(payload) ?
                state.checkedInventoryList.filter(p => p.id !== payload.id) :
                [...state.checkedInventoryList, payload]
            return {
                ...state,
                singleProduct : null,
                checkedInventoryList: inventoryProducts
            };
        case ActionTypes.SELECT_OR_DESELECT_INVENTORY:
            return {
                ...state,
                checkedInventoryList: payload
            };
        case ActionTypes.REMOVE_FROM_INVENTORY:
            const removedInventory = state.inventory.filter(p => !(state.checkedInventoryList.includes(p)))
            return {
                ...state,
                inventory: removedInventory,
                checkedInventoryList: []
            };
        case ActionTypes.FILTER_INVENTORY:

            return {
                ...state,
                filteredInventory: filter(state.inventory, state.filterInventoryList),
                checkedInventoryList: []
            };
        case ActionTypes.SEARCH_INVENTORY:
            return {
                ...state,
                filterInventoryList:{
                    ...state.filterInventoryList,
                    search: payload
                }
            };
        case ActionTypes.SORT_INVENTORY:
            return {
                ...state,
                filterInventoryList:{
                    ...state.filterInventoryList,
                    sort: payload
                }
            };
        case ActionTypes.SET_MIN_PRICE_INVENTORY:
            return {
                ...state,
                filterInventoryList:{
                    ...state.filterInventoryList,
                    slideMin: payload
                }
            };
        case ActionTypes.SET_MAX_PRICE_INVENTORY:
            return {
                ...state,
                filterInventoryList:{
                    ...state.filterInventoryList,
                    slideMax: payload
                }
            };
        case ActionTypes.SET_ALERT:
            return {
                ...state,
               alert: payload
            };
        case ActionTypes.SET_ALERT_TYPE:
            return {
                ...state,
                alertType: payload
            };
        default:
            return state;
    }
}

import {ActionTypes} from "../contants/action-types";

export const countryNames =  (products) => {
    return {
        type: ActionTypes.COUNTRIES_LIST,
        payload: products
    }
}
export const selectCountry =  (products) => {
    return {
        type: ActionTypes.SELECTED_COUNTRY,
        payload: products
    }
}
export const searchCountry =  (products) => {
    return {
        type: ActionTypes.SEARCH_COUNTRY,
        payload: products
    }
}
export const filterCountries =  (products) => {
    return {
        type: ActionTypes.FILTER_COUNTRIES,
        payload: products
    }
}
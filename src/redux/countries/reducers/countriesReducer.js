import {ActionTypes} from "../contants/action-types";


const initialState = {
    countries: [],
    country: '',
    filteredCountries: [],
}
export const countriesReducer = (state = initialState, {type, payload}) => {

    switch (type){
        case ActionTypes.COUNTRIES_LIST:

            return {
                ...state,
                countries: payload,
                filteredCountries: payload
            };
        case ActionTypes.SELECTED_COUNTRY:

            return {
                ...state,
                selectedCountry: payload
            };
        case ActionTypes.SEARCH_COUNTRY:
            return {
                ...state,
                country: payload,
                selectedCountry: undefined,
            };
        case ActionTypes.FILTER_COUNTRIES:
            let filteredCountries = state.countries
            filteredCountries = filteredCountries.filter(item => {
                return (item.toUpperCase().indexOf(state.country.toUpperCase()) > -1)
            })
            return {
                ...state,
                filteredCountries: filteredCountries
            };
        default:
            return state;
    }
}

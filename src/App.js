import './App.css';
import './components/Nav/Nav.css'
import './components/Aside/Aside.css'
import './fonts/font.css';
import Products from "./components/Main/Products";
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import './App.css';
import Nav from "./components/Nav/Nav";
import Profile from "./components/profile/Profile"
import React, {useEffect, useState} from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import Category from "./components/Category";
import Dashboard from "./components/dashboard/Dashboard";
import Inventory from "./components/inventory/Inventory";
import ShoppingCart from "./components/shoppingCart/ShoppingCart";
import Login from "../src/Login/Login"
import {createMuiTheme,  MuiThemeProvider} from "@material-ui/core/styles";
import {useDispatch, useSelector} from "react-redux";
import SignUp from "./components/Register/SignUp";
import VerifyEmail from "./components/Register/VerifyEmail";
import {setIsLogged} from "./redux/users/actions/usersActions";
import CreateProduct from "./components/CreateProduct/CreateProduct";
import EditProduct from "./components/EditProduct/EditProduct";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import {setAlert, setAlertType} from "./redux/products/actions/prodactActions";
import Slide from "@material-ui/core/Slide";

const theme = createMuiTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 640,
            md: 935,
            lg: 1367,
            xl: 1599,
        },
    },
    palette: {
        text: {
            primary: "#c0c0c0",
        },
    },
})
function App() {

    const dispatch = useDispatch();
    const isLogged = useSelector((state) => state.users.isLogged)
    const alert = useSelector((state) => state.products.alert)
    const alertType = useSelector((state) => state.products.alertType)
    const [transition, setTransition] = useState(undefined);
    const [open, setOpen] = useState(false);

    useEffect(() => {
        if(localStorage.getItem("token")) {
            dispatch(setIsLogged(true))
        }
    }, )
    useEffect(() => {
           if(alert === "") setOpen(false)
           else{setOpen(true)}
           setTransition(() => TransitionLeft);
    }, [alert])
    const handleClose = () => {
        setOpen(false);
        dispatch(setAlert(""))
        dispatch(setAlertType(""))
    };
    const TransitionLeft = (props) => {
        return <Slide {...props} direction="left" />;
    }
    return (
      <MuiThemeProvider theme={theme}>
          <Snackbar open={open}
                    autoHideDuration={3000}
                    onClose={handleClose}
                    TransitionComponent={transition}
          >
              <Alert onClose={handleClose} variant="filled" severity={(alertType)? alertType : "success"}>
                  {alert}
              </Alert>
          </Snackbar>
      <Router>
    <div className="App">
        {(isLogged) ?
            (
                <>

                <Nav/>
            <Switch>
                <Route path={"/profile"}>
                        <Profile/>
                </Route>
                <Route path={"/dashboard"}>
                        <Dashboard/>
                </Route>
                <Route path={"/catalog/:id?"}>
                    <Aside/>
                    <div className={"catalog"}>
                        <Header/>
                        <Products/>
                    </div>
                </Route>
                <Route path={"/inventory"}>
                        <Inventory/>
                </Route>
                <Route path={"/cart"}>
                   <ShoppingCart/>
                </Route>
                <Route path={"/orders"}>
                    <></>
                </Route>
                <Route path={"/transactions"}>
                    <></>
                </Route>
                <Route path={"/storesList"}>
                    <></>
                </Route>
                <Route path={"/category/:name"} component={Category}/>
                <Route path={"/create-product"}>
                    <CreateProduct/>
                </Route>
                <Route path={"/edit-product"}>
                    <EditProduct/>
                </Route>
            </Switch>
                </>
            )
            :(
            <Switch>
            <Route exact path={"/"}>
            <Redirect to={"/login"}/>
            </Route>
            <Route path={"/login"}>
            <Login/>
            </Route>
            <Route path={"/register"}>
            <SignUp/>
            </Route>
            <Route path={"/verify-email"}>
            <VerifyEmail/>
            </Route>
            </Switch>
            )
        }

    </div>
      </Router>
      </MuiThemeProvider>
  );
}

export default App;

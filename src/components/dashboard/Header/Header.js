import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
import React from "react";

const Header = () => {
    return (
        <div className={"header__wrapper-dashboard"}>
            <div className={"page__title"}>
                <span>Dashboard</span>
            </div>
            <div className={"current-plan"}>
              <span>current plan</span>
                <button>Free</button>
            </div>
            <div className={"help dashboard--help"}>
                <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
            </div>
        </div>
    )
}
export default Header
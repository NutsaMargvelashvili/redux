import AD from "../../../images/ad.png"
import {useState} from "react";

const Main = () => {

    const WorldMap = require('react-world-map');
    const [selected, onSelect] = useState(null);

    return (
        <div className={"main__wrapper"}>
            <div className="dashboard">
                <p>Welcome Nutsa Margvelashvili, you’re almost ready to go!!</p>
                <p>Follow these steps to get started</p>
                <div className="instructions">
                <div className="steps">
                    <div className="step-1">
                        <div className="step_illustrate">
                            <img src={"https://app.365dropship.com/assets/images/svg/wizard1.svg"} alt={"profile details"}/>
                        </div>
                        <h3>1. Complete your profile details</h3>
                        <span>Finalize your account set-up, choose your plan and fill in your payment method</span>
                    </div>
                    <div className="step-2">
                        <div className="step_illustrate">
                            <img src={"https://app.365dropship.com/assets/images/svg/wizard2.svg"} alt={"explore catalog"}/>
                        </div>
                        <h3>2. Explore catalog and add items to your inventory</h3>
                        <span>Fill in your inventory, easily filter and search to find the products most suitable for you</span>
                    </div>
                    <div className="step-3">
                        <div className="step_illustrate">
                            <img src={"https://app.365dropship.com/assets/images/svg/wizard3.svg"} alt={"eCommerce store"}/>
                        </div>
                        <h3>3. Add your eCommerce store</h3>
                        <span>Integrate one or more eCommerce stores to your account</span>
                    </div>
                    <div className="step-4">
                        <div className="step_illustrate">
                            <img src={"https://app.365dropship.com/assets/images/svg/wizard4.svg"} alt={"explore"}/>
                        </div>
                        <h3>4. Synchronize/Export your inventory</h3>
                        <span>Synchronize the products in your inventory to your eCommerce store and start generating sales</span>
                    </div>
                </div>
                <button className={"start_BTN"}>start</button>
                <button className={"skip_BTN"}>skip</button>
            </div>
            </div>
            <div className="advertising">
                <img src={AD} alt={"ad"}/>
            </div>
            <div className="countries-table">
                <table>
                    <thead>
                    <tr>
                        <th>countries</th>
                        <th>suppliers</th>
                        <th>products</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>🇺🇸 United States</td>
                        <td>23</td>
                        <td>53,011</td>
                    </tr>
                    <tr>
                        <td>🇬🇧 United Kingdom</td>
                        <td>8</td>
                        <td>14,952</td>
                    </tr>
                    <tr>
                        <td>🇵🇱 Poland</td>
                        <td>5</td>
                        <td>31,369</td>
                    </tr>
                    <tr>
                        <td>🇩🇪 Germany</td>
                        <td>5</td>
                        <td>4,247</td>
                    </tr>
                    <tr>
                        <td>🇮🇹 Italy</td>
                        <td>5</td>
                        <td>18,892</td>
                    </tr>
                    <tr>
                        <td>🇨🇳 China</td>
                        <td>5</td>
                        <td>7,056</td>
                    </tr>
                    <tr>
                        <td>🇪🇸 Spain</td>
                        <td>4</td>
                        <td>105,024</td>
                    </tr>
                    <tr>
                        <td>🇳🇱 Netherlands</td>
                        <td>3</td>
                        <td>12,932</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div className="map">
                <WorldMap selected={ selected } onSelect={ onSelect } />
            </div>
        </div>
    )
}
export default Main
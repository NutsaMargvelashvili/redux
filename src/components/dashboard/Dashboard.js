import "./Dashboard.css"
import Main from "./Main/Main";
import Header from "./Header/Header";
const Dashboard = () => {
    return (
        <div className={"dashboard__wrapper"}>
        <Header/>
        <Main/>
        </div>
    )
}
export default Dashboard
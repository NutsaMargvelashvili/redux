import "../../Login/Login.css"
import React, {useEffect, useState} from "react";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import {Checkbox, TextField} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {useDispatch, useSelector} from "react-redux";
import { setToken, setUser} from "../../redux/users/actions/usersActions";
import {useHistory} from "react-router-dom";
import {init} from 'emailjs-com';
import * as emailjs from "emailjs-com";
import * as yup from "yup";
import {useFormik} from "formik";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import {registerUser, getCart} from "../../API";

init("user_zimqcdsVpIA1XhNugOrw3");

const SignUp = () => {

    const history = useHistory()
    const dispatch = useDispatch();
    const token = useSelector((state) => state.users.users.token)
    const [passwordVisible, setPasswordVisible] = useState(false);
    const [validationCode, setValidationCode] = useState()

    let templateParams = {
        to_Email: 'nutsamargvelashvili@gmail.com',
        from_name: '365Dropship',
        message_html: 'Please Find out the attached file',
        validation_code: validationCode,
        user_token: token,
    };
    const validationSchema = yup.object({
        Email: yup
            .string('Enter your email')
            .email('Enter a valid email')
            .required('Email is required'),
        Password: yup
            .string('Enter your password')
            .min(6, 'Password should be of minimum 6 characters length')
            .required('Password is required'),
    })
    const formik = useFormik({
        initialValues: {
            Email: "",
            Password: ""
        },
        validationSchema: validationSchema,
        onSubmit:  (values) => {
            registerUser( values.Email, values.Password).then(
                (user) => {
                    dispatch(setUser(JSON.stringify(user.data)))
                    dispatch(setToken(user.data.data.token))
                }
            ).catch((err) => {
                alert(err);
            })
            sendEmail(values);
        }
    })
    const sendEmail = () =>{
        setValidationCode((Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000).toString())
    }
    const handleSignInBTN = () => {
        history.push('/login')
    }
    const handleClickShowPassword = () => {
        setPasswordVisible(!passwordVisible);
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    useEffect(() => {

        console.log("token ---> " + token + templateParams.user_token)
        if(validationCode && token){
            emailjs.send('service_xfpx9fm', 'template_ps2fxqn', templateParams)
                .then(function(response) {
                    localStorage.setItem("code", validationCode)
                    console.log('SUCCESS!', response.status, response.text);
                }, function(error) {
                    console.log('FAILED...', error);
                });
            history.push('/verify-email')
        }

    }, [templateParams.validation_code])
    useEffect(() => {
        getCart()
    }, [])

    const useStyles = makeStyles((theme) => ({
        root: {
            '&:hover': {
                backgroundColor: 'transparent',
            },
        },
        icon: {
            borderRadius: 1,
            width: 16,
            height: 16,
            border: '1px solid #e7e7e7',
            backgroundColor: '#FFF'
        },
        textField: {
            '& > *': {
                marginBottom: theme.spacing(4),
                width: '100%',
                fontSize: '13px',
                color: '#000!important',
                fontWeight: '500',
                backgroundColor: "#FFF",
            },
            "& label.Mui-focused": {
                borderColor: "revert",
            },
            "& .MuiInput-underline:after": {
                borderColor: "revert",
            },
            "& .MuiOutlinedInput-root": {
                "& fieldset": {
                    borderColor: "revert",
                },
                "&:hover fieldset": {
                    borderColor: theme.palette.primary,
                },
                "&.Mui-focused fieldset": {
                    borderColor: "revert",
                    borderWidth: '1px!important'
                },
            },
        }}));

    const classes = useStyles();
    const { handleSubmit, errors, touched, values } = formik;

    return(
        <div className={"login"}>
            {/*    */}
            <form className={"form__wrapper--sign-up"} >
                <div className={"login-header"}>
                    <img alt={"Logo"} src={"https://app.365dropship.com/assets/images/dropship_logo.png"}/>
                    <span>Sign Up</span>
                </div>
                <div className={"login-form"}>
                        <TextField
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <MailOutlineIcon className={"email_icon"}/>
                                    </InputAdornment>
                                ),
                            }}
                            className={classes.textField}
                            style={{width: '100%'}}
                            value={values.Email}
                            variant="outlined"
                            type={"text"}
                            name={"Email"}
                            id={"Email"}
                            placeholder={"E-mail"}
                            error={touched.Email && Boolean(errors.Email)}
                            onChange={(e) => {formik.handleChange(e)}}/>
                        <TextField
                            type={passwordVisible ? 'text' : 'password'}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon className={"password_icon"}/>
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {passwordVisible ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                            name={"Password"}
                            placeholder={"Password"}
                            error={touched.Password && Boolean(errors.Password)}
                            value={values.Password}
                            color="none"
                            style={{width: '100%'}}
                            className={classes.textField}
                            id={"Password"}
                            variant="outlined"
                            onChange={(e) => {formik.handleChange(e)}}/>
                    <p>By creating an account, you agree with the <a target={"_blank"} rel="noreferrer" href={"https://www.365dropship.com/terms-of-service/"}>Terms & Conditions</a> and <a rel="noreferrer" target={"_blank"} href={"https://www.365dropship.com/terms-of-service/"}>Privacy Policy</a></p>
                    <div className={"subscribe"}>
                        <Checkbox
                                        defaultChecked
                                        color="default"
                                        inputProps={{ 'aria-label': 'checkbox with default color' }}
                                        checked
                        />
                        <label>Subscribe to Newsletter</label>
                    </div>

                    <input type={"submit"} value={"Log In"} onClick={(e) => {handleSubmit(e)}}/>
                    <div className={"social_auths"}>
                        <hr/>
                        <span>Or Sign In With</span>
                    </div>
                    <div className={"social_BTNs"}>
                        <button><img src={"https://app.365dropship.com/gmail.285cd2a6d2400e92b9c8.png"} alt={"google icon"}/></button>
                        <button><img src={"https://app.365dropship.com/facebook.298647c6d4d58b0ec5a1.png"} alt={"Facebook icon"}/></button>
                    </div>
                    <div className={"sign_up"}>
                        <span>Already have an account?</span>
                        <button type="button" onClick={handleSignInBTN}>Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default SignUp
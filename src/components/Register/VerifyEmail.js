import "../../Login/Login.css"
import React, {useEffect} from "react";
import {useDispatch} from "react-redux";
import {setIsLogged} from "../../redux/users/actions/usersActions";
import {useHistory} from "react-router-dom";
import {init} from 'emailjs-com';
import {getCart} from "../../API";

init("user_zimqcdsVpIA1XhNugOrw3");

const SignUp = () => {

    const history = useHistory()
    const dispatch = useDispatch();
    const verifyEmail = window.location.href.split("email/")[1];
    const verifyToken = window.location.href.split("/")[5];
    const local = localStorage.getItem("code")
    let validationCode;
    let userCode;

    const sendEmail = (e) =>{
        e.preventDefault();
        validationCode = (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000).toString()
    }

    useEffect(() => {
        setTimeout(() => {
            console.log(local, verifyEmail, typeof(local), local === `"${verifyEmail}"`)
        }, 2000)
        console.log(localStorage.getItem("code") , verifyEmail, local === `"${verifyEmail}"`)
        if(verifyEmail && (localStorage.getItem("code") === local)) {
            localStorage.setItem("token", verifyToken)
            dispatch(setIsLogged(true))
            history.push('/profile')
        }
    }, [verifyEmail])
    useEffect(() => {
        getCart()
    }, [])

    return(
        <div className={"login"}>
            <form className={"emailVerification"}>
                <img src={"https://app.365dropship.com/assets/images/auth/envelop.jpg"} alt={"verify email"}/>
                <h2>Thanks, You're All Set!</h2>
                <p>An email confirming your registration was just sent to</p>
                <input type={"text"}  onChange={(e) => {userCode = e.target.value}}/>
                <button type={"button"}></button>
                <button onClick={(e) => {sendEmail(e)}}>Resend Verification Email</button>
            </form>
        </div>
    )
}

export default SignUp
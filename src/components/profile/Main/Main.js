import Button from "../../Button/Button"
import React, {useCallback, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import Countries from "../Countries";
import {filterCountries, selectCountry} from "../../../redux/countries/actions/countriesActions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronDown, faChevronUp} from "@fortawesome/free-solid-svg-icons";
import {searchCountry} from "../../../redux/countries/actions/countriesActions";
import {debounce} from "lodash";
import {updateUserData} from "../../../API";
import {setAlert} from "../../../redux/products/actions/prodactActions";

const Main = () => {
    const dispatch = useDispatch();
    const countriesList = useSelector((state) => state.countries.filteredCountries)
    const country = useSelector((state) => state.countries.selectedCountry)
    const [dropdownVisible, setDropdownVisible] = useState(false)
    const firstName = localStorage.getItem("firstName")
    const lastName = localStorage.getItem("lastName")
    const id = localStorage.getItem("id")
    /*************/
    const delayQuery = useCallback(
        debounce(async (searchText) => {
            await dispatch(searchCountry(searchText))
            await dispatch(filterCountries())
        }, 800),
        []);
    /*****************/
    const setCountry = (name) => {
        dispatch(selectCountry(name))

    }
    const handleChanges = () => {
       return updateUserData(id).then(() => {
           dispatch(setAlert("The changes saved"))
           })
    }


    return (
      <div className={"profile_form"}>
        <div className={"form__header"}>
            <div>
                <span>profile</span>
                <span>billing</span>
                <span>invoice history</span>
            </div>
            <button>deactivate account</button>

        </div>
          <div className={"profile"} >
              <div className={"profile-picture"}>
                  <p>profile picture</p>
                  <div className="form">
                      <div className={"picture--frame"}> <img src={"https://app.365dropship.com/assets/images/profile-example.jpg"} alt={"frame"}/></div>
                      <Button ultraBig title={"upload"}/>
                  </div>
              </div>
              <div className={"personal-details"}>
                  <p>personal details</p>
                  <div className="form">
                      <form>
                          <label htmlFor={"firstName"}>First Name</label>
                           <input placeholder={"First Name"} value={firstName} type={"text"} id={"firstName"} required/>
                          <label htmlFor={"lastName"}>Last Name</label>
                          <input placeholder={"Last Name"} value={lastName} id={"lastName"} type={"text"} required/>
                          <label htmlFor={"country"}>Country</label>
                          <div className="select_country">
                          </div>
                          <div className="select_country">
                              <input onChange={async (event) => {delayQuery(event.target.value)}} onClick={() => setDropdownVisible(true)} placeholder={"Country"} value={country && country} id={(country !== "")? "selectedCountry" : "Country"} type={"text"} required/>
                              <span>
                                  <FontAwesomeIcon className={"chevronDown--category country-dropdown" +((dropdownVisible? "": " chevronDownVisible"))} icon={faChevronDown} />
                                  <FontAwesomeIcon className={"chevronUp--category country-dropdown" +((dropdownVisible? " chevronUpVisible": ""))} icon={faChevronUp}/></span>
                              <div onClick={() => setDropdownVisible(false)}  className={"dropdown_countries" + (dropdownVisible? " dropdown_countries--visible": "")}>
                                  <ul className={"countries__list"}>
                                      {(countriesList && countriesList[0]) && countriesList.map((country) =>
                                          <Countries  kay={country} chooseCountry={(name) => setCountry(name)} name={country}/>
                                      )}
                                  </ul>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
              <div className={"change-password"}>
                  <p>change password</p>
                  <div className="form">
                      <form>
                          <label htmlFor={"currentPassword"}>Current Password</label>
                          <input type={"password"} id={"currentPassword"} required/>
                          <label htmlFor={"newPassword"}>New Password</label>
                          <input id={"newPassword"} type={"password"} required/>
                          <label htmlFor={"confirmPassword"}>Confirm New Password</label>
                          <input id={"confirmPassword"} type={"password"} required/>
                      </form>
                  </div>
              </div>
              <div className={"contact-information"}>
                  <p>contact information</p>
                  <div className="form">
                      <form>
                          <label htmlFor={"email"}>Email</label>
                          <input placeholder={"email"} type={"text"} id={"email"} required/>
                          <label htmlFor={"skype"}>Skype</label>
                          <input placeholder={"skype"} id={"skype"} type={"text"} required/>
                          <label htmlFor={"phone"}>Phone</label>
                          <input placeholder={"phone"} id={"phone"} type={"text"} required/>
                      </form>
                  </div>
              </div>
              <div className={"save_BTN"}>
                  <Button title={"Save Changes"} extraBig buttonClicked={handleChanges}/>
              </div>
          </div>

      </div>
    )
}

export default Main
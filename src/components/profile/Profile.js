import React, {useEffect} from "react";
import "./Profile.css"
import Header from "./Header/Header";
import Main from "./Main/Main"
import axios from "axios";
import {countriesSerializer} from "../../utils/serializers";
import {useDispatch} from "react-redux";
import {countryNames} from "../../redux/countries/actions/countriesActions";

const Profile = () => {
    const dispatch = useDispatch();

    const fetchCountries = async () => {
        const response = await axios
            .get('https://restcountries.eu/rest/v2/all')
            .catch(err => {
                console.log("ERROR", err)
            })
        dispatch(countryNames(countriesSerializer(response.data)))
    }
    useEffect(() => {
        fetchCountries()
    }, [])
    return (
        <div className={"profile__wrapper"}>
            <Header/>
            <div className="main__wrapper">
                <Main/>
            </div>
        </div>
    )
}

export default Profile
import React from "react";

const Countries = ({name, chooseCountry}) =>{
    return (
      <li onClick={() => chooseCountry(name)}>{name}</li>
    )

}
export default Countries
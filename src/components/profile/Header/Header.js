import React from "react";
import "../Profile.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
import {useDispatch} from "react-redux";
import {setIsLogged, setToken, setUser} from "../../../redux/users/actions/usersActions";
import {useHistory} from "react-router-dom";
const Header = () =>{
    const dispatch = useDispatch();
    const history = useHistory()
    return (
        <div className={"header__wrapper"}>
        <span>My Profile</span>
        <button className={"sign-out"} onClick={()=>{
            dispatch(setUser(""))
            dispatch(setToken(""))
            localStorage.removeItem("token")
            dispatch(setIsLogged(false))
            history.push("/login")
        }}>sign out</button>
            <div className={"help profile--help"}>
                <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
            </div>
        </div>
    )
}

export default Header
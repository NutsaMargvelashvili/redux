import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import "./CreateProduct.css"
import {Form, Formik, Field} from "formik";
import axios from "axios";
import * as yup from "yup";
import {createProduct} from "../../API";
import {setAlert} from "../../redux/products/actions/prodactActions";
import {useDispatch} from "react-redux";

const validationSchema = yup.object().shape({
    title: yup.string().min(2).max(20),
    description: yup.string().min(5).max(500),
    price: yup.number().integer().min(50),
    imageUrl: yup.string().url()
})
const CreateProduct = () => {
    const dispatch = useDispatch();
    axios.interceptors.request.use((config)=>{
    config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
    return config;
})
const handleSubmit = values => {

    return createProduct(values).then(() => {
            dispatch(setAlert("The product has been created"))
        })
    }
    return (
            <div className={"create-product__wrapper"}>
            <div className={"header__wrapper"}>
                <span>Create Product</span>
                <div className={"help profile--help"}>
                    <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
                </div>
            </div>
              
                <div className={"create-product__form"}>
                    <Formik initialValues={{
                        title: "",
                        description: "",
                        price: "",
                        imageUrl: ""
                    }
                    }
                            onSubmit={handleSubmit}
                            validationSchema={validationSchema}
                    >
                    <Form style={{display: 'flex', flexDirection: "column", justifyContent: "center", alignItems: "center", padding: "20px"}}>
                     <Field placeholder={"Title"}
                            name={"title"}
                            className={"form-input"}
                     />
                    <Field placeholder={"Description"}
                           name={"description"}
                           className={"form-input"}
                    />
                    <Field placeholder={"price"}
                           type={"number"}
                           name={"price"}
                           className={"form-input"}
                    />
                    <Field placeholder={"Image URL"}
                           name={"imageUrl"}
                           className={"form-input"}
                    />
                        <input type={"submit"} value={"save"}/>
                    </Form>
                    </Formik>
                </div>
            </div>

    )
}
export default CreateProduct
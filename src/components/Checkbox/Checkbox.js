import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import RadioButtonUncheckedRoundedIcon from '@material-ui/icons/RadioButtonUncheckedRounded';

const CustomColorCheckbox = withStyles({
    root: {
        color: '#ECEDF5',
        '&$checked': {
            color: '#61d5df',
        },
        marginLeft: "20px",
        marginTop: "10px",
        padding: "10px"
    },
    checked: {},
})((props) => <Checkbox color='default' {...props} />);

export function CheckboxLabels({selectedCatalogItems, checkboxHandler }) {
    return (
        <FormControlLabel
            control={
                <CustomColorCheckbox
                    onChange={checkboxHandler}
                    name='checked'
                    color='primary'
                    checked={selectedCatalogItems}
                    checkedIcon={<CheckCircleRoundedIcon />}
                    icon={<RadioButtonUncheckedRoundedIcon />}
                />
            }
        />
    );
}

export default CheckboxLabels
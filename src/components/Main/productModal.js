import "./Products.css"
import React, {useState} from "react";
import Button from "../Button/Button";
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes} from '@fortawesome/free-solid-svg-icons'
import {selectedProducts, setAlert} from "../../redux/products/actions/prodactActions";
import Magnifier from "react-magnifier";
import {useHistory} from "react-router-dom";
import {Modal} from "@material-ui/core";
import {deleteProduct} from "../../API";

const ProductModal = ({product}) => {
    const dispatch = useDispatch();
    const history = useHistory()
    const selectedProduct = useSelector((state) => state.products.singleProduct)
    const [detailsOpt, setDetailsOpt] = useState("product details")
    const isAdmin = localStorage.getItem("isAdmin")
    const handleRemoveProduct = id => {
        return deleteProduct(id).then(() => {
            dispatch(setAlert("The product has been removed"))
            window.location.href = `/catalog`
        })
    }

    return (
        <Modal outline={"none"} className={"Modal"} onClose={() => {dispatch(selectedProducts()); history.push("/catalog")}} open>
            <div className={"product--opened" + ((selectedProduct)? "-clicked": "")}>
                    <div className={"item-clicked"}>
                        <div className={"item--info"}>
                            <div className={"item--price"}>
                                               <div className="catalog__price-zoomed">
                                                                   <div className="RRP">
                                                                                                     <p>$10</p>
                                        <p>RRP</p>
                                    </div>
                                    <div className="Profit">
                                        <p className={"description"}>${product.price}</p>
                                        <p>cost
                                        </p>
                                        <div className={"separator"}></div>
                                    </div>
                                    <div className="Cost">
                                        <p>60% ($6)</p>
                                        <p>profit</p>
                                        <div className={"separator"}></div>
                                    </div>
                                </div></div>
                            <div className={"item--image"}>

                                    <div className={"zoom-wrapper"}>


                                            {/*<img className={"zoomImage"} src={product.image} alt={"Product"}/>*/}
                                        {/*<GlassMagnifier*/}
                                        {/*    className={"zoomImage"}*/}
                                        {/*    imageSrc={product.image}*/}
                                        {/*    imageAlt="Example"*/}
                                        {/*    // largeImageSrc="./large-image.jpg" // Optional*/}
                                        {/*/>*/}

                                        <Magnifier mgMouseOffsetX={0} mgMouseOffsetY={0} mgBorderWidth={0} zoomFactor={0.1} mgHeight={1100} mgWidth={1100} mgTouchOffsetY={0} mgTouchOffsetX={0} src={product.imageUrl} mgShape={"square"} className={"zoomImage"} />

                                </div>
                            </div>
                        </div>
                        <div className={"item--description"}>
                            <div  className={"closeModal"}><button onClick={() => {dispatch(selectedProducts()); history.push("/catalog")}}><FontAwesomeIcon icon={faTimes} /></button></div>
                            <p className={"copy"}><span>SKU# bgb-s2412499 COPY</span>
                                <span  className={"item--supplier"}>supplier: <a href={"https://app.365dropship.com/catalog"}>US-Supplier146</a></span>
                            </p>
                               <p className={"title"}>{product.title}</p>
                            <div className={"addToInventory"}><Button title={"add to my inventory"} extraBig/></div>
                            <div className={"details"}>
                                <p>
                                    <span id={(detailsOpt === "product details")? "activeOpt":""} onClick={()=>setDetailsOpt("product details")}>product details</span>
                                    <span id={(detailsOpt === "shipping rates")? "activeOpt":""} onClick={()=>setDetailsOpt("shipping rates")}>shipping rates</span>
                                    <span id={(detailsOpt === "available options")? "activeOpt":""} onClick={()=>setDetailsOpt("available options")}>available options</span>
                                </p>
                            </div>
                            <div className={"description__wrapper"}>
                                {(detailsOpt === "product details") &&  <p className={"desc"}>{product.description}</p>}
                                {(detailsOpt === "shipping rates") &&
                                    <table className={"product__table"}>
                                        <tr>
                                            <th>Country</th>
                                            <th>Regular / Days</th>
                                            <th>Express / Days</th>
                                        </tr>
                                        <tr>
                                            <td>Austria</td>
                                            <td>$14.46 / 5-9</td>
                                            <td>N/A / N/A</td>
                                        </tr>
                                        <tr>
                                            <td>Belgium</td>
                                            <td>$14.46 / 5-9</td>
                                            <td>N/A / N/A</td>
                                        </tr>
                                    </table>
                                }
                                {(detailsOpt === "available options") &&
                                <table className={"product__table"}>
                                    <tr>
                                        <th>Size</th>
                                        <th>Quantity</th>
                                    </tr>
                                    <tr>
                                        <td>M</td>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>XXL</td>
                                        <td>3</td>
                                    </tr>
                                    <tr>
                                        <td>XL</td>
                                        <td>3</td>
                                    </tr>
                                </table>
                                }
                            </div>
                            {isAdmin === "true" && <div style={{display: "flex", justifyContent: "space-between", width: "100%"}}>

                                <Button title={"Remove The Product"} ultraBig buttonClicked={() => {
                                    handleRemoveProduct(product.id)
                                }}/>
                                <Button title={"Edit The Product"} ultraBig buttonClicked={() => {
                                    window.location.href = `/edit-product/${product.id}`
                                }}/>
                            </div>}
                        </div>
                    </div>
                </div>

            </Modal>
    );

}

export default ProductModal;
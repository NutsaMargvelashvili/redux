import "./Products.css"
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import Product from "./Product";
import {selectedProducts, setProducts} from "../../redux/products/actions/prodactActions";
import ProductModal from "./productModal";
import Load from "../../images/load.gif"
import loadProduct from "../../images/loadProduct.gif"
import Button from "../Button/Button";
import {useHistory, useParams} from "react-router-dom";
import LinkOffIcon from '@material-ui/icons/LinkOff';
import Grid from '@material-ui/core/Grid';
import {fetchProducts, fetchProduct} from "../../API";

const Products = () => {
    const dispatch = useDispatch();
    const history = useHistory()
    const productList = useSelector((state) => state.products.filteredProducts)
    const products = useSelector((state) => state.products.products)
    const selectedProduct = useSelector((state) => state.products.singleProduct)
    const isLoading = useSelector((state) => state.products.isLoading)
    const [linkValid, setLinkValid] = useState(true);
    const [profit, setProfit] = useState();
    const {id} = useParams()

    const handleButton = () => {
            history.push("/catalog")
            setLinkValid(true)
        }
    useEffect(() => {
        fetchProducts().then(
            response => {if(!productList.length) dispatch(setProducts(response.data.data))})
    }, [])
    useEffect(() => {
        id && fetchProduct(id).then(response => {
            if(response == null) setLinkValid(false)
            else dispatch(selectedProducts(response.data.data))
        });
    }, [id])
    useEffect(() => {
       setProfit(Math.random() * 60);
    }, [])

    return(
            <Grid container
                  spacing={2}
                  justify="flex-start"
                  alignItems="flex-start"
                  className={"main_wrapper"}
            >
                {(linkValid)?
                (products && products[0])?
                    productList.map((product) =>
                        <Grid key={product.id} xs={12} sm={6} lg={4} xl={3} item>
                        <Product product={product} profit={Math.round(profit)}/>
                    </Grid>)
                : <>
                    <Grid container
                          spacing={2}
                          justify="flex-start"
                          alignItems="flex-start"
                          className={"main_wrapper"}
                          style={{overflow: "hidden"}}
                    >
                        { [...Array(20)].map((el, index) => {
                            return (
                                <Grid style={{height: 450}} key={index}  xs={12} sm={6} lg={4} xl={3} item>
                            <div className={"placeholder"}><img alt={"skeleton"} src={loadProduct}/></div>
                                </Grid>
                            )
                        })
                        }
                    </Grid>)
                    </>: <div className={"brokenLink"}>
                        <LinkOffIcon fontSize="large"></LinkOffIcon>
                        <h2>This page isn't available</h2>
                        <p>The link may be broken, or the page may have been removed.
                            Check to see if the link you're trying to open is correct.</p>
                        <Button title={"go to home page"} buttonClicked={handleButton} big></Button>
                    </div>}

                {isLoading && <div className={"transparent-clicked"}><img src={Load} alt={"loading"}/></div>}
                    {selectedProduct && <ProductModal product={selectedProduct} /> }
            </Grid>
    );

}

export default Products;
import {useDispatch, useSelector} from "react-redux";
import {
    selectedProducts,
    checkedProducts, setAlert, setAlertType,
} from "../../redux/products/actions/prodactActions";
import Button from "../Button/Button";
import { useHistory } from "react-router-dom";
import {useState} from "react";
import {addToCart, deleteProduct} from "../../API";
import Checkbox from "../Checkbox/Checkbox"
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';

const Product = ({product, profit, load}) => {
    const dispatch = useDispatch();
    const checkedProduct = useSelector((state) => state.products.checkedProductsList)
    const [productQuantity, setProductQuantity] = useState(1)
    const handleProduct = (e) => {
        if (e.target.type !== "checkbox"){
            dispatch(selectedProducts(product));
            history.push(`/catalog/${product.id}`);
        }
    }
    const handleRemoveProduct = id => {
        return deleteProduct(id).then(() => {
            dispatch(setAlert("The product has been removed"))
            window.location.href = `/catalog`
        })
    }
    const handleCheckbox = (e) => {
        dispatch(checkedProducts(product));
        e.stopPropagation();
    }
    const handleAlert = () => {
        dispatch(setAlert("The product has been added to the inventory"))
    }
    const history = useHistory()
    return(
        <div className={"product__item" + (((checkedProduct.includes(product)))? " product__item--highlighted": "")} onClick={handleProduct}>
            {(load)? <p>Loading...</p>:""}

            <div className={"product--head" + ((checkedProduct.includes(product))? " product--head--visible": "")}>
                <div className={"checkbox-cont"}>
                    <Checkbox   selectedCatalogItems={(checkedProduct.includes(product))} checkboxHandler={handleCheckbox}/>
                </div>
                <div className={"btn-area" + ((checkedProduct.includes(product))? " product--head--invisible": "")}>
                    <div className={"product--buttons"}>
                        <Button buttonClicked={() => {handleRemoveProduct(product.id)}} title={<DeleteOutlineIcon/>}/>
                        <Button buttonClicked={() => {window.location.href = `/edit-product/${product.id}`}} title={<EditIcon/>}/>
                        <Button buttonClicked={()=>{addToCart(product.id, productQuantity).then(() => {handleAlert()})}} title={"Add To Inventory"}/>
                    </div>
                </div>
            </div>


            <div  className="catalog__product">
                <div className="catalog__photo">
                    <img src={product.imageUrl}  alt={"product proto"}/>
                </div>
                <div className="catalog__title">
                    <p>{product.title}</p>
                    <div className={"choose--quantity"} >
                        <input type={"number"} min={"1"} onClick={(e)=>{e.preventDefault(); e.stopPropagation(); setProductQuantity(parseInt(e.target.value) || productQuantity)}}/>
                    </div>
                </div>
                <div className={"supplier"}>
                    <p>By: <a href={"https://app.365dropship.com/catalog"}>US-Supplier146</a></p>
                </div>
                <div className="catalog__price">
                    <div className="RRP">
                        <p>${Math.round((product.price * 100) / (100 - profit))}</p>
                        <p>RRP</p>
                    </div>
                    <div className="Profit">
                        <p>${product.price}</p>
                        <p>cost
                        </p>
                        <div className={"separator"}></div>
                    </div>
                    <div className="Cost">
                        <p>{profit}% (${Math.round((product.price * profit) / (100 - profit))})</p>
                        <p>profit</p>
                        <div className={"separator"}></div>
                    </div>
                </div>
            </div>


        </div>
    );

}

export default Product;
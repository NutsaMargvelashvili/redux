import React from "react";
import "../ShoppingCart.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
const Header = () =>{
    return (
        <div className={"header__wrapper"}>
            <span>shopping cart (0)</span>
            <div className={"help cart--help"}>
                <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
            </div>
        </div>
    )
}

export default Header
import React from "react";
import "../ShoppingCart.css"

const Main = () => {

    return (
        <div className={"cart_form"}>
            <div className={"cart"} >
                <div className={"cart-details"}>
                    <ul>
                        <li>ITEM DESCRIPTION</li>
                        <li>SUPPLIER</li>
                        <li>OPTIONS</li>
                        <li>QUANTITY</li>
                        <li>ITEM COST</li>
                        <li>VAT</li>
                        <li>TOTAL COST</li>
                    </ul>
                    </div>
                </div>
            </div>
    )
}

export default Main
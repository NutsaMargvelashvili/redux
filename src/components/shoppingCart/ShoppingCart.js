import "./ShoppingCart.css"
import Header from "./Header/Header";
import Main from "./Main/Main"

const ShoppingCart = () => {
    return (
        <div className={"cart__wrapper"}>
            <Header/>
            <div className="main__wrapper">
                <Main/>
            </div>
        </div>
    )
}

export default ShoppingCart
import Aside from "./Aside/Aside";
import Header from "./Header/Header";
import Products from "./Main/Products";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {filterProducts, selectCategory} from "../redux/products/actions/prodactActions";

const Category = (props) => {
    const products = useSelector((state) => state.products.products)
    let category = (props.match.params.name.includes("category"))?
                          props.match.params.name.replace("category", "")
                        : props.match.params.name.replace("-", " ")
    const dispatch = useDispatch();

    useEffect(() => {
        if(products){
            dispatch(selectCategory(category))
            dispatch(filterProducts())
        }
    }, [category, products])

    return (
        <>
            <Aside />
            <div className={"catalog"}>
                <Header />
                <Products />
            </div>
        </>
    )
}

export default Category;
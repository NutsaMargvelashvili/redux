const Slider = ({setSlideMin, setSlideMax, min, max, maxVal, minVal}) => {

    return (
        <>
            <input
                type="range"
                min={min}
                max={max}
                value={minVal}
                onChange={event => {
                    setSlideMin(event.target.value)
                }}
                className="thumb thumb--left"
            />
            <input
                type="range"
                min={min}
                max={max}
                value={maxVal}
                onChange={event => {
                    setSlideMax(event.target.value)
                }}
                className="thumb thumb--right"
            />
            <div className="slider">
                <div className="slider__track" />
                <div className="slider__range" />
            </div>
        </>
    );
};


export default Slider
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));
const SuccessAlert = (text, onClose) => {
    const classes = useStyles();
    return(
        <div className={classes.root}>
        <Alert severity="success" color="info" onClose={onClose}>
            {text}
        </Alert>
        </div>
    )
}

export default  SuccessAlert
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestion} from "@fortawesome/free-solid-svg-icons";
import React, {useEffect, useState} from "react";
import "./EditProduct.css"
import {Field, Form, Formik} from "formik";
import * as yup from "yup";
import {editProduct, fetchProduct} from "../../API";
import {useDispatch} from "react-redux";
import {setAlert} from "../../redux/products/actions/prodactActions";

const validationSchema = yup.object().shape({
    title: yup.string().min(2).max(20),
    description: yup.string().min(5).max(500),
    price: yup.number().integer().min(50),
    imageUrl: yup.string().url()
})

const CreateProduct = () => {
    const id = window.location.href.split("/")[4];
    const [productValues, setProductValues] = useState()
    const dispatch = useDispatch();

    const handleSubmit = values => {
        return editProduct(values, id).then(() => {
            dispatch(setAlert("The product has been updated"))
        })
    }

    useEffect(() => {
        fetchProduct(id).then(result => setProductValues(result.data.data))
    }, [])

    return (

        <div className={"edit-product__wrapper"}>
            <div className={"header__wrapper"}>
                <span>Edit Product</span>
                <div className={"help profile--help"}>
                    <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
                </div>
            </div>

            <div className={"edit-product__form"}>
                <Formik initialValues={{
                    title: (productValues)? productValues.title : "",
                    description: (productValues)? productValues.description : "",
                    price: (productValues)? productValues.price : "",
                    imageUrl: (productValues)? productValues.imageUrl : "",
                }
                }
                        enableReinitialize={true}
                        onSubmit={handleSubmit}
                    // validationSchema={validationSchema}
                >
                    <Form style={{display: 'flex', flexDirection: "column", justifyContent: "center", alignItems: "center", padding: "20px"}}>
                        <Field placeholder={"title"}
                               name={"title"}
                               className={"form-input"}
                               // placeholder={(product)? product.title : ""}
                        />
                        <Field placeholder={"Description"}
                               name={"description"}
                               className={"form-input"}
                        />
                        <Field placeholder={"price"}
                               type={"number"}
                               name={"price"}
                               className={"form-input"}
                        />
                        <Field placeholder={"Image URL"}
                               name={"imageUrl"}
                               className={"form-input"}
                        />
                        <input type={"submit"} value={"save"} />
                    </Form>
                </Formik>
            </div>
        </div>

    )
}
export default CreateProduct
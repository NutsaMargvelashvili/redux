import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faSort} from '@fortawesome/free-solid-svg-icons'
import React, { useState } from 'react';
import { MdSort } from 'react-icons/md';
import {useDispatch} from "react-redux";
import {filterProducts, sortProducts} from "../../redux/products/actions/prodactActions";
const Sort = () => {
    const dispatch = useDispatch();
    const [getVal, setVal] = useState(false);
    const [getSortVal, setSortVal] = useState("New Arrivals");
    const dropdownDisplay = () => {
        setVal(!getVal);
    }
    const sortable = (event) => {
        setSortVal(event.target.innerHTML);
        dispatch(sortProducts(event.target.innerHTML))
        dispatch(filterProducts())
    }
    return(
        <div className={"nav_sort"}>
            <ul className={"sort__list"}>
                <li className="sort" value={getVal} onClick={dropdownDisplay}>
                    <p><MdSort style={{fontSize: 18}} className={"Icon" + ((getVal)? "--darker": "")} /></p>
                    <p className={"sortBy"}>Sort By: </p><p className={"sorted"}>{(getSortVal)}</p>
                    <ul className={"dropdown" + ((getVal)? " visible": "")}>
                        <li className={"" + ((getSortVal === "New Arrivals")? "active": "")}><p onClick={sortable}>New Arrivals</p></li>
                        <li className={"" + ((getSortVal === "Price: Low to High")? "active": "")}><p onClick={sortable}>Price: Low to High</p></li>
                        <li className={"" + ((getSortVal === "Price: High to Low")? "active": "")}><p  onClick={sortable}>Price: High to Low</p></li>
                        <li className={"" + ((getSortVal === "From A to Z")? "active": "")}><p onClick={sortable}>From A to Z</p></li>
                        <li className={"" + ((getSortVal === "From Z to A")? "active": "")}><p onClick={sortable}>From Z to A</p></li>
                    </ul>
                    <p> <FontAwesomeIcon className={"Icon" + ((getVal)? "--darker": "")} icon={faSort}/></p>

                </li>
            </ul>

        </div>

    );

}
export default Sort;
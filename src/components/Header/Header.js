import React from "react";
import "./Header.css"
import Search from "./Search";
import Sort from "./Sort";
import Button from "../Button/Button";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faQuestion} from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import {useDispatch, useSelector} from "react-redux";
import {
    addToInventory,
    selectOrDeselect, setAlert, setAlertType
} from "../../redux/products/actions/prodactActions";
import {addToCart} from "../../API";

const Header = () => {
    const dispatch = useDispatch();
    const productList = useSelector((state) => state.products.filteredProducts)
    const checkedItems = useSelector((state) => state.products.checkedProductsList)

    const selectAll = () => {
        dispatch(selectOrDeselect(productList))
    }
    const deselectAll = () => {
        dispatch(selectOrDeselect([]))
    }
    const addCheckedItems = async () => {
        let result = [];
        if(checkedItems.length){
            for (const product of checkedItems) {
                result = await addToCart(product.id, 1);
            }
            if (result) {
                dispatch(addToInventory(result.data.data.cartItem.items));
                dispatch(selectOrDeselect([]))
                dispatch(setAlert("The products have been added to the inventory"))
            }
        }
        else{
            dispatch(setAlert("You should select at least one product"))
            dispatch(setAlertType("error"))
        }
    };
    return (
        <div className={"header_wrapper"}>
            <div className="Header">
                <div className={"aside__filter-small"}>
                    <Button title={"filter"}/>
                </div>
                <div className={"selected__products-number"}>
                    <Button  title={"select all"}  buttonClicked={selectAll}/>
                     <p> selected {checkedItems.length} out of {productList.length} products</p>
                    <Button title={"clear"} buttonClicked={deselectAll} clearBTN />
                </div>
                <div className={"selected__products-number-small"}>
                    <button className={"button--small"}><FontAwesomeIcon icon={faCheckCircle} /></button>
                </div>
                <div  className={"nav-search"}>
                    <Search />
                    <Button buttonClicked={addCheckedItems} title={"add to inventory"} big/>
                    <div className={"menu"}>
                        <button className={"menu__BTN"}>  <FontAwesomeIcon icon={faBars} /></button>
                    </div>
                    <div className={"help"}>
                        <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
                    </div>

                </div>
            </div>
            <div className={"Header__sort-filter"}>
                <Sort />
            </div>
        </div>

    )
}

export default Header;
import {useSelector} from "react-redux";

const Button = ({big, ultraBig, extraBig, buttonClicked, clearBTN, title, clearBTNInventory}) => {
    const checkedInventory = useSelector((state) => state.products.checkedInventoryList)
    const checkedProduct = useSelector((state) => state.products.checkedProductsList)
    const buttonStyle = () => {
        if(big){

            return " button--big";
        }
        else if(ultraBig){

            return " button--Ultra-Big";
        }
        else if(extraBig){
            return " button--extra-Big";
        }
        else if(clearBTN){
            return ((checkedProduct.length > 0)? " clearBTN--visible":" clearBTN--invisible");
        }
        else if(clearBTNInventory){
            return ((checkedInventory.length > 0)? " clearBTN--visible":" clearBTN--invisible");
        }
        else{
            return ""
        }
    }
    const onClick = e => {
        e.stopPropagation();
        buttonClicked()
    }
    return(
        <button className={"button" + buttonStyle()} onClick={onClick}>{title}</button>
    );

}
export default Button;
import "./Nav.css"
import React, {useEffect, useState} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faTachometerAlt} from '@fortawesome/free-solid-svg-icons'
import {faListUl} from '@fortawesome/free-solid-svg-icons'
import {faDiceD6} from '@fortawesome/free-solid-svg-icons'
import {faShoppingCart} from '@fortawesome/free-solid-svg-icons'
import {faClipboardCheck} from '@fortawesome/free-solid-svg-icons'
import {faExchangeAlt} from '@fortawesome/free-solid-svg-icons'
import {faClipboardList} from '@fortawesome/free-solid-svg-icons'
import {faPlusSquare} from '@fortawesome/free-solid-svg-icons'
import {Link} from "react-router-dom";
import {useLocation} from 'react-router-dom'
import {faEdit} from "@fortawesome/free-solid-svg-icons";


const Nav = () => {
    const [selectedNavItem, setSelectedNavItem] = useState("profile");
    const isAdmin = localStorage.getItem("isAdmin")
    const location = useLocation()
    const url = location.pathname;

        useEffect(() => {
            setSelectedNavItem(url);
        }, [url])
    return (
        <div className={"Nav_wrapper"}>
            <nav>
                <div className={"logo"}>
                    <img alt={"Logo"} src={"https://app.365dropship.com/assets/images/dropship_logo.png"}/>
                </div>
                <ul className={"nav__list"}>
                    <li className={"nav__list-item" + ((selectedNavItem === "/profile")? " active": "")}> <Link to="/profile"><div className={"img--frame"}><img alt={"Profile"} src={"https://app.365dropship.com/assets/images/profile-example.jpg"}/></div> </Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/dashboard")? " active": "")}> <Link to="/dashboard"><FontAwesomeIcon icon={faTachometerAlt} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/catalog")? " active": "")}> <Link to="/catalog"><FontAwesomeIcon icon={faListUl} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/inventory")? " active": "")}> <Link to="/inventory"><FontAwesomeIcon icon={faDiceD6} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/cart")? " active": "")}> <Link to="/cart"><FontAwesomeIcon icon={faShoppingCart} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/orders")? " active": "")}> <Link to="/orders"><FontAwesomeIcon icon={faClipboardCheck} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/transactions")? " active": "")}> <Link to="/transactions"><FontAwesomeIcon icon={faExchangeAlt} /></Link></li>
                    <li className={"nav__list-item" + ((selectedNavItem === "/storesList")? " active": "")}> <Link to="/storesList"><FontAwesomeIcon icon={faClipboardList} /></Link></li>
                    {isAdmin === "true" &&
                    <>
                        <li className={"nav__list-item" + ((selectedNavItem === "/create-product")? " active": "")}> <Link to="/create-product"><FontAwesomeIcon icon={faPlusSquare} /></Link></li>
                        {/*<li className={"nav__list-item" + ((selectedNavItem === "/edit-product")? " active": "")}> <Link to="/edit-product"><FontAwesomeIcon icon={faEdit} /></Link></li>*/}
                    </>
                    }

                </ul>
            </nav>
        </div>
    )
}
export default Nav;
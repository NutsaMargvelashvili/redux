import {useDispatch, useSelector} from "react-redux";
import {
    selectedProducts,
    checkedInventoryProducts
} from "../../../redux/products/actions/prodactActions";
import Button from "../../Button/Button";
import axios from "axios";
import {removeFromCart} from "../../../API";
import Checkbox from "../../Checkbox/Checkbox";
import {useState} from "react";

const InventoryProducts = ({product, profit}) => {
    const checkedProduct = useSelector((state) => state.products.checkedInventoryList)
    const [productQuantity, setProductQuantity] = useState(product.qty)
    const dispatch = useDispatch();
    axios.interceptors.request.use((config)=>{
        config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
        return config;
    })
    const removeItem = (id) => {
        removeFromCart(id).then(() => {
            window.location.href = "/inventory"
        }).catch((err) => {
            if(err.response.status === 401){
                localStorage.removeItem("token");
                localStorage.removeItem("code")
                window.location.href = "/login"
            }
        })
    }
    return(
        <div className={"product__item" + (((checkedProduct.includes(product)))? " product__item--highlighted": "")} onClick={() => dispatch(selectedProducts(product))}>
            <div className={"product--head" + ((checkedProduct.includes(product))? " product--head--visible": "")}>
                <div className={"checkbox-cont"}>
                    <Checkbox   selectedCatalogItems={(checkedProduct.includes(product))} checkboxHandler={() => dispatch(checkedInventoryProducts(product))}/>
                </div>
                <div className={"btn-area" + ((checkedProduct.includes(product))? " product--head--invisible": "")}>
                    <div>
                        <Button title={"Remove From Inventory"} buttonClicked={()=>{removeItem(product.id)}}/>
                    </div>

                </div>
            </div>


            <div  className="catalog__product">
                <div className="catalog__photo">
                    <img src={product.image}  alt={"product proto"}/>
                </div>
                <div className="catalog__title">
                    <p>{product.title}</p>
                    <div className={"quantity"}>
                        <span> Quantity: {product.qty}</span>
                    </div>

                </div>
                <div className={"supplier"}>
                    <p>By: <a href={"https://app.365dropship.com/catalog"}>US-Supplier146</a></p>
                    <div className={"choose--quantity"} >
                        <input type={"number"} min={"1"} value={productQuantity} onClick={(e)=>{e.preventDefault(); e.stopPropagation(); setProductQuantity(parseInt(e.target.value) || productQuantity)}}/>
                    </div>
                </div>
                <div className="catalog__price">
                    <div className="RRP">
                        <p>${Math.round((product.price * 100) / (100 - profit))}</p>
                        <p>RRP</p>
                    </div>
                    <div className="Profit">
                        <p>${product.price}</p>
                        <p>cost
                        </p>
                        <div className={"separator"}></div>
                    </div>
                    <div className="Cost">
                        <p>{profit}% (${Math.round((product.price * profit) / (100 - profit))})</p>
                        <p>profit</p>
                        <div className={"separator"}></div>
                    </div>
                </div>
            </div>


        </div>
    );

}

export default InventoryProducts;
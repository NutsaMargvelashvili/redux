import "../../Main/Products.css"
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import InventoryProducts from "./InventoryProducts";
import {addToInventory} from "../../../redux/products/actions/prodactActions";
import {Link} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import {cartProducts} from "../../../API";

const Main = () => {
    const inventoryList = useSelector((state) => state.products.filteredInventory)
    const dispatch = useDispatch();
    const [profit, setProfit] = useState();
    useEffect(() => {
        setProfit(Math.random() * 60);
    }, [])
    useEffect(()=>{
        cartProducts().then(
            result => {
                dispatch(addToInventory(result.data.data.cartItem.items))
            }
        )
    }, [])
    return(
            <>
            {(inventoryList && inventoryList[0])?
                 <Grid container
                       spacing={2}
                       justify="flex-start"
                       alignItems="flex-start"
                       className={"main_wrapper"}>
                     { inventoryList.map((inventory) =>  <Grid key={inventory.id} xs={12} sm={6} lg={4} xl={3} item><InventoryProducts  product={inventory} profit={Math.round(profit)}/></Grid>)}
                </Grid>
                : <>
                    <div className={"empty-inventory"}>
                        <img src={"https://app.365dropship.com/assets/images/svg/empty_inventory.svg"} alt={"empty box"}/>
                        <p>Your inventory is currently empty, to add products to sync, choose from the <Link to="/catalog">catalog</Link></p>
                    </div>
                </>
            }
            </>
    );


}

export default Main;
import "./Inventory.css"
import "../../App.css"
import Header from "./Header/Header";
import Main from "./Main/Main";
import Aside from "./Aside/Aside";
import React from "react";
const Inventory = () =>{
    return(
        <div className={"inventory__wrapper"}>
            <Aside/>
            <div className={"catalog"}>
            <Header/>
            <Main/>
            </div>
        </div>
    )
}
export default Inventory
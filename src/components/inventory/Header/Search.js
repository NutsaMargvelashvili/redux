import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import {filterInventory, searchInventory} from "../../../redux/products/actions/prodactActions";
import {useDispatch} from "react-redux";
import {useState,useCallback} from "react";
import { debounce } from 'lodash'

const Search = () => {
    const dispatch = useDispatch();
    const [searchInputVal, setSearchInputVal] = useState("");

    const delayQuery = useCallback(
        debounce(async (searchText) => {
            setSearchInputVal(searchText)
            await dispatch(searchInventory(searchText))
            await dispatch(filterInventory())
        }, 300),
        []);

    const searchInput = () => {
        console.log(searchInputVal, "   searchasjfkadsjfnskj")
        dispatch(searchInventory(searchInputVal))
        dispatch(filterInventory())
    }
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            searchInput()
        }
    }
    return(
        <div className={'Header__search'}>
            <div className="Search">
                <input type={'text'} placeholder={'search...'} onKeyDown={handleKeyDown} className={"Search__products"} onChange={async (event) => {delayQuery(event.target.value)}}/>
                <button className={'Search__ICON'} >
                    <FontAwesomeIcon icon={faSearch} onClick={searchInput}/>
                </button>
            </div>
        </div>

    );

}
export default Search;

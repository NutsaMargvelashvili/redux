import React from "react";
import "../../Header/Header.css"
import Search from "./Search";
import Sort from "./Sort";
import Button from "../../Button/Button";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faQuestion} from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import {useDispatch, useSelector} from "react-redux";
import {
    addToInventory,
    removeFromInventory,
    selectOrDeselectInventory, setAlert, setAlertType
} from "../../../redux/products/actions/prodactActions";
import {removeFromCart} from "../../../API";

const Header = () => {
    const dispatch = useDispatch();
    const productList = useSelector((state) => state.products.filteredInventory)
    const checkedProducts = useSelector((state) => state.products.checkedInventoryList)

    const removeCheckedItems= async () => {
        let result = [];
        if(checkedProducts.length){
            for (const product of checkedProducts) {
                result = await removeFromCart(product.id);
                dispatch(removeFromInventory(product.id));
            }
            if (result) dispatch(addToInventory(result.data.data.cartItem.items));
        }
        else{
            dispatch(setAlert("You should select at least one product"))
            dispatch(setAlertType("error"))
        }
    };
    const selectAll = () => {
        dispatch(selectOrDeselectInventory(productList))
    }
    const deselectAll = () => {
        dispatch(selectOrDeselectInventory([]))
    }
    return (
        <div className={"header_wrapper"}>
            <div className="Header">
                <div className={"aside__filter-small"}>
                    <Button title={"filter"}/>
                </div>
                <div className={"selected__products-number"}>
                    <Button  title={"select all"}  buttonClicked={selectAll}/>
                    <p> selected {checkedProducts.length} out of {productList.length} products</p>
                    <Button title={"clear"} buttonClicked={deselectAll} clearBTNInventory/>
                </div>
                <div className={"selected__products-number-small"}>
                    <button className={"button--small"}><FontAwesomeIcon icon={faCheckCircle} /></button>
                </div>
                <div  className={"nav-search"}>
                    <Search />
                    <Button title={"export products"} big/>
                    <Button title={"add to cart"} big/>
                    <Button buttonClicked={removeCheckedItems} title={"remove from my inventory"} big/>
                    <div className={"menu"}>
                        <button className={"menu__BTN"}>  <FontAwesomeIcon icon={faBars} /></button>
                    </div>
                    <div className={"help inventory-help"}>
                        <button className={"help__BTN"}>   <FontAwesomeIcon icon={faQuestion} /></button>
                    </div>

                </div>
            </div>
            <div className={"Header__sort-filter"}>
                <Sort />
            </div>
        </div>

    )
}

export default Header;
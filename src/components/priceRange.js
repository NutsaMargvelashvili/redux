export const priceRange = (payload) => {
    let prices = {
        min: 0,
        max: 0
    }
    prices.min = Math.floor(Math.min.apply(Math, payload.map((item) => { return item.price; })))
    prices.max = Math.ceil(Math.max.apply(Math, payload.map((item) => { return item.price; })))
    return prices
}
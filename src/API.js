import axios from "axios";

const URL = "http://18.185.148.165:3000/"
const URL_V1 = "http://18.185.148.165:3000/api/v1/"

axios.interceptors.request.use((config)=>{
    config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
    return config;
})

export const registerUser = async (registeredEmail, registeredPassword) => {
       return await axios.post(URL + "register", {firstName: "nkjnk", lastName: "nkl", email: registeredEmail, password: registeredPassword, passwordConfirmation: registeredPassword})
}

export const cartProducts = async () => {
    try{
        return await axios.get(URL_V1 + "cart")
    }catch(err){
        if(err.response.status === 401){
            localStorage.removeItem("token");
            localStorage.removeItem("code")
            window.location.href = "/login"
        }
    }
}

export const fetchProducts = async () => {
    return await axios
        .get(URL_V1 + 'products')
        .catch(err => {
            console.log("ERROR", err)
        });
}

export const fetchProduct = async (id) => {
    return await axios
        .get(`${URL_V1}products/${id}`)
        .catch(err => {
            alert(err)
        })
}
export const login = async (email, password) => {
   return await axios.post(URL + "login", {email: email, password: password})
}
export const getCart = () => {
    axios.get(URL_V1 + "cart").then(r => console.log(r))
}
export const removeFromCart = async (productId) => {
    return await axios.post(URL_V1 + `cart/remove/${productId}`)
}

export const editProduct = async (data, id) => {
    return await axios.put(URL_V1 + `products/${id}`, data)
}
export const createProduct = async (data) => {
    return await axios.post(URL_V1 + "products", data)
}
export const deleteProduct = async (id) => {
    // alert("The product has been removed")
    return await axios.delete(URL_V1 + `products/${id}`)
}

export const updateUserData = async (id) => {
    return await axios
        .put(URL_V1 + `users/${id}`, {firstName: "salome", lastName: "margvelashvili", email: "salomemargvelashvili2@gmail.com", password: "secret"})
        .catch(err => {
            console.log("ERROR", err)
        });
}
export const addToCart = async (productId, qty) => {
    return await axios.post(URL_V1 + "cart/add", {productId, qty})
}
export const filter = (products, filterProductList) => {
    let filtered = products
    //sort
    if(filterProductList.sort === "Price: High to Low"){
        filtered.sort((a, b) => (a.price < b.price) ? 1 : -1)
    }
    else if(filterProductList.sort === "Price: Low to High"){
        filtered.sort((a, b) => (a.price > b.price) ? 1 : -1)
    }
    else if(filterProductList.sort === "From A to Z"){
        filtered.sort((a, b) => (a.title > b.title) ? 1 : -1)
    }
    else if(filterProductList.sort === "From Z to A"){
        filtered.sort((a, b) => (a.title < b.title) ? 1 : -1)
    }
    else if(filterProductList.sort === "New Arrivals"){
        filtered.sort((a, b) => (a.id > b.id) ? 1 : -1)
    }
    // filter && search

    if ((Number(filterProductList.slideMin) > Number(filterProductList.slideMax))) {
        filtered = filtered.filter(product => {
            return ((product.price <= filterProductList.slideMin) &&
                (product.price >= filterProductList.slideMax) &&
                (product.title.toUpperCase().includes(filterProductList.search.toUpperCase()))
                // (product.category.includes(filterProductListcategory)) &&
                // !(inventory.includes(product)))
            )
        })
    } else {
        filtered = filtered.filter(product => {
            return ((product.price >= filterProductList.slideMin) &&
                (product.price <= filterProductList.slideMax) &&
                (product.title.toUpperCase().includes(filterProductList.search.toUpperCase()))
                // (product.category.includes(filterProductListcategory)) &&
                // !(inventory.includes(product)))
            )
        })
    }
    return filtered
}
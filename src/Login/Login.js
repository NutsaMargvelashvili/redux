import "./Login.css"
import React, {useEffect, useState} from "react";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { makeStyles } from '@material-ui/core/styles';
import {useDispatch } from "react-redux";
import {setIsLogged, setToken, setUser} from "../redux/users/actions/usersActions";
import {useHistory} from "react-router-dom";
import {useFormik} from "formik";
import * as yup from 'yup'
import {TextField} from "@material-ui/core";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import {login} from "../API";
import loader from "../images/loader.gif"

const validationSchema = yup.object({
    Email: yup
        .string('Enter your email')
        .email('Enter a valid email')
        .required('Email is required'),
    Password: yup
        .string('Enter your password')
        .min(6, 'Password should be of minimum 6 characters length')
        .required('Password is required'),
})
const Login = () => {
    const history = useHistory()
    const dispatch = useDispatch();
    const [passwordVisible, setPasswordVisible] = useState(false);
    const [isLoading, setIsLoading] = useState(false)

    const formik = useFormik({
            initialValues: {
                Email: "",
                Password: ""
            },
    onSubmit: (values) => {
        login(values.Email, values.Password).then( user => {
            setIsLoading(true)
            setTimeout(()=>{
                setIsLoading(false)
                dispatch(setUser(JSON.stringify(user.data)))
                dispatch(setToken(JSON.stringify(user.data.data.token)))
                dispatch(setIsLogged(true))
                localStorage.setItem("token", user.data.data.token)
                localStorage.setItem("isAdmin", user.data.data.isAdmin)
                localStorage.setItem("id", user.data.data.id)
                localStorage.setItem("firstName", user.data.data.firstName)
                localStorage.setItem("lastName", user.data.data.lastName)
                history.push("/profile")
            },2000)

        }).catch((err)=>{
            alert("Invalid email or password! Try again")
        })
    },
            validationSchema: validationSchema
        })


    const handleClickShowPassword = () => {
        setPasswordVisible(!passwordVisible);
    };
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const handleSignUp = () => {
        history.push("/register")
    }

    useEffect(() => {
        const token = localStorage.getItem("token")
        if(token) {
            history.push('/profile')
            dispatch(setIsLogged(true))
        }
    }, [])
    const useStyles = makeStyles((theme) => ({
        root: {
            '& > *': {
                marginBottom: theme.spacing(4),
                width: '100%',
                fontSize: '13px',
                color: '#000!important',
                fontWeight: '500',
                backgroundColor: "#FFF",
            },
            "& label.Mui-focused": {
                borderColor: "revert",
            },
            "& .MuiInput-underline:after": {
                borderColor: "revert",
            },
            "& .MuiOutlinedInput-root": {
                "& fieldset": {
                    borderColor: "revert",
                },
                "&:hover fieldset": {
                    borderColor: theme.palette.primary,
                },
                "&.Mui-focused fieldset": {
                    borderColor: "revert",
                    borderWidth: '1px!important'
                },
            },
        },
    }));
    const { handleSubmit, errors, touched, values } = formik;
    const classes = useStyles();

    return(
        <div className={"login"}>

            {(isLoading)?


                <img src={loader} alt={"loading"}/>:
                ( <form className={"form__wrapper"} >
                    <div className={"login-header"}>
                        <img alt={"Logo"} src={"https://app.365dropship.com/assets/images/dropship_logo.png"}/>
                        <span>Members Log In</span>
                    </div>
                    <div className={"login-form"}>
                        <TextField
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <MailOutlineIcon className={"email_icon"}/>
                                    </InputAdornment>
                                ),
                            }}
                            style={{width: '100%'}}
                            disableRipple
                            className={classes.root}
                            variant="outlined"
                            value={values.Email}
                            type={"text"} name={"Email"}
                            id={"Email"}
                            placeholder={"E-mail"}
                            error={touched.Email && Boolean(errors.Email)}
                            // helperText={touched.Email && errors.Email}
                            onChange={(e) => {formik.handleChange(e)}}/>
                        <TextField
                            type={passwordVisible ? 'text' : 'password'}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <VpnKeyIcon className={"password_icon"}/>
                                    </InputAdornment>
                                ),
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {passwordVisible ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                ),
                            }}
                            error={touched.Password && Boolean(errors.Password)}
                            value={values.Password}
                            color="none"
                            style={{width: '100%'}}
                            className={classes.root}
                            variant="outlined"
                            name={"Password"}
                            id={"Password"}
                            placeholder={"Password"}
                            onChange={(e) => {formik.handleChange(e)}}/>
                        <a href={"https://www.google.com"}>Forgot  Password?</a>
                        <input type={"submit"} value={"Log In"}  onClick={handleSubmit}/>
                        <div className={"social_auths"}>
                            <hr/>
                            <span>or Log In With</span>
                        </div>
                        <div className={"social_BTNs"}>
                            <button><img src={"https://app.365dropship.com/gmail.285cd2a6d2400e92b9c8.png"} alt={"google icon"}/></button>
                            <button><img src={"https://app.365dropship.com/facebook.298647c6d4d58b0ec5a1.png"} alt={"Facebook icon"}/></button>
                        </div>
                        <div className={"sign_up"}>
                            <span>Don't have an account?</span>
                            <button type="button" onClick={handleSignUp}>Sign up</button>
                        </div>
                    </div>
                </form>)
            }
        </div>
    )
}



export default Login